package main

import "os"

func main() {
	sample()
	sampleOsExit()
}

// defer は、それ以降の行で return, panic が起きても必ず最後に実行される
// file.Close() など、リソースの解放を必ず行うために使われる
func sample() {
	println("1")
	defer println("2")
	println("3")
	return
	println("4")
	defer println("5")
	println("6")
}

// os.Exit は defer を無視してプログラムを終了する
func sampleOsExit() {
	println("1")
	defer println("2")
	println("3")
	os.Exit(0)
	println("4")
	defer println("5")
	println("6")
}
