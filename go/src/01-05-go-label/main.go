package main

// 通常のbreak文は、所属する最も内側の「for」文、「switch」文、「select」文から抜け出す
// func main() {
// 	for i := 0; i < 10; i++ {
// 		switch {
// 		case i == 3:
// 			break

// 		default:
// 			println(i)
// 		}
// 	}
// }

// 「for」文、「switch」文、「select」文が入れ子になったときに使える
func main() {
FOR_LABEL:
	for i := 0; i < 10; i++ {
		switch {
		case i == 3:
			break FOR_LABEL

		default:
			println(i)
		}
	}
}
