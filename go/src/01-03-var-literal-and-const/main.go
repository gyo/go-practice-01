package main

func main() {
	name := "Mr.Go"
	println(name)

	const title = "Go言語入門"
	// title = "DUMMY"
	println(title)

	var i = 77
	var f = 77.7
	var c = 7i
	var s = 'ル'            // ルーン：コードポイントを表現する整数
	var r = `raw文字列\nリテラル` // \n は "\" "n" の 2 文字
	var d = "通常の\n文字列"     // \m は 改行コード

	println(i, f, c, s, r, d)
}
