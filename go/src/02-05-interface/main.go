package main

func main() {
	str := "STRING"
	dog := new(Dog)
	cat := new(Cat)
	MakeAnimalCry(dog)
	MakeAnimalCry(cat)
	MakeSomeoneCry(dog)
	MakeSomeoneCry(str)
}

type Animal interface {
	Cry()
}

func MakeAnimalCry(a Animal) {
	println("鳴け")
	a.Cry()
}

type Dog struct{}

func (d *Dog) Cry() {
	println("わんわん")
}

type Cat struct{}

// たとえばこの関数をコメントアウトすると、*Cat does not implement Animal (missing Cry method)
// インタフェースが必要とするメソッドをすべて実装した時点で、自動的にそのインタフェースを実装したとみなされる
func (c *Cat) Cry() {
	println("にゃーにゃー")
}

// interface{} 型はなんの振る舞いも要求しない
// すべてのオブジェクトは interface{} 型を実装していることになる

func MakeSomeoneCry(someone interface{}) {
	println("鳴け")
	// someone.Cry()
	// someone.Cry undefined (type interface {} is interface with no methods)
	// Animal 型とみなしてメソッドを実行するには、型アサーションという仕組みを使う

	a, ok := someone.(Animal)

	if !ok {
		println("動物ではないので鳴けません")
		return
	}

	a.Cry()
}
