package main

// 構造体型
type Dictionary struct {
	name    string
	meaning string
}

// 関数型
type ReadFunc func(Dictionary) string

type Score int

// 関数のレシーバ型
func (s Score) Show() {
	println(s)
}

func main() {
	var readFunc ReadFunc
	var dict Dictionary

	readFunc = readOut
	dict.name = "コーヒー"
	dict.meaning = "コーヒー豆から作られる黒色の飲み物"
	// dict.dummy = "DUMMY"

	println(readFunc(dict))

	var myScore Score = 100
	myScore.Show()

	println(int(myScore))
}

func readOut(d Dictionary) string {
	return d.name + d.meaning
}
