package main

import "some"

func main() {
	println(some.Apple)
	some.Hello()
	some.Apple = "Cherry"
	some.Hello()
	println("main")
}
