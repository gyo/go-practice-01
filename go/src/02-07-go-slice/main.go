package main

import "fmt"

func main() {
	num := [5]int{1, 2, 3, 4, 5}
	fmt.Println(num)

	var slice1 []int
	slice1 = num[:]
	fmt.Println(slice1)
	fmt.Println(len(slice1))
	fmt.Println(cap(slice1))

	var slice2 []int
	slice2 = num[1:4]
	fmt.Println(slice2)

	slice3 := slice1[1:4]
	fmt.Println(slice3)
	fmt.Println(len(slice3))
	fmt.Println(cap(slice3))
}
