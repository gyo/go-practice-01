package vector

type Vector3 struct {
	X int
	Y int
	z int // 小文字なので、パッケージ外部からアクセスできない
}

// 小文字なので、パッケージ外部からアクセスできない
type vector3 struct {
	X int
	Y int
	z int
}

func ExportPrivate() vector3 {
	var v vector3
	return v
}
