package main

import "fmt"

func main() {
	// 配列の中身は同じ型じゃないといけない
	// 長さは固定
	var a [1]byte
	var b [10]int
	var c [12]struct{ a, b rune } // 構造体
	var d [2][8]rune
	var month [3]string
	month[0] = "January"
	month[1] = "February"
	month[2] = "March"
	var arr = [...]string{"One", "Two", "THree"}

	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(d)
	for i := 0; i < len(month); i++ {
		println(month[i])
	}
	for _, m := range month {
		println(m)
	}
	println(month[1])
	// 下のコードは、エラーになる nvalid array index 5 (out of bounds for 3-element array)
	// println(month[5])
	for _, item := range arr {
		println(item)
	}
}
