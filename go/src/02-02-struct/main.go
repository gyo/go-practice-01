package main

import "fmt"
import "vector"

type Vector struct {
	X int
	Y int
}

func main() {
	v2 := Vector{
		X: 3,
		Y: 7,
	}

	v3 := vector.Vector3{
		X: 1,
		Y: 5,
		// z: 9,
	}

	// unexported := vector.vector3{
	// 	X: 1,
	// 	Y: 5,
	// 	z: 9,
	// }

	exported := vector.ExportPrivate()
	exported.X = 2
	exported.Y= 4
	// exported.z = 6

	fmt.Println(v2)
	fmt.Println(v3)
	// fmt.Println(unexported)
	fmt.Println(exported)
}
