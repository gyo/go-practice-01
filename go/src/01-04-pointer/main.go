package main

func main() {
	// ポインタ変数
	var pointer *int
	// int 型変数
	var n int = 100

	// n のアドレスを pointer に代入
	// 値をアドレスに変換するには &
	pointer = &n

	println(n)
	println(pointer)
	println(&n)
	println(*pointer)

	// ポインタ渡しのサンプル
	a, b := 10, 10

	called(a, &b)

	println(a)
	println(b)
}

func called(a int, b *int) {
	a = a + 1
	// アドレスを値に変換するには *
	*b = *b + 1
}
