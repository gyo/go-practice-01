package main

func main() {
	DisplayHello()
	DisplaySum(1, 2)
	DisplaySumAll(1, 2, 3, 4, 5)
	println(Sum(10, 20))
	result, remainder := Div(19, 4)
	println(result, remainder)
	var three LoopNum = 3
	three.TimeDisplay("Hello")

	var box *SavingBox = NewBox()
	box.Income(100)
	box.Income(200)
	box.Income(300)
	println(box.Break())
	box.Income(400)
	println(box.Break())
}

func DisplayHello() {
	println("Hello")
}

func DisplaySum(left int, right int) {
	println(left + right)
}

func DisplaySumAll(values ...int) {
	var sum = 0
	for _, value := range values {
		sum += value
	}
	println(sum)
}

func Sum(left int, right int) int {
	return left + right
}

func Div(left int, right int) (int, int) {
	return left / right, left % right
}

// レシーバ変数
type LoopNum int

// レシーバ変数のメソッド
func (n LoopNum) TimeDisplay(s string) {
	for i := 0; i < int(n); i++ {
		println(s)
	}
}

type SavingBox struct {
	money int
}

func NewBox() *SavingBox {
	return new(SavingBox)
}

func (s *SavingBox) Income(amount int) {
	s.money += amount
}

func (s *SavingBox) Break() int {
	lastMoney := s.money
	s.money = 0
	return lastMoney
}
