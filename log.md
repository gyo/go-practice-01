```
tamuragyonoMacBook-puro:go-practice-01 tamuragyo$ docker build -t my-golang-app .
Sending build context to Docker daemon  4.608kB
Step 1/9 : FROM alpine:3.8
Step 2/9 : RUN apk add --no-cache               ca-certificates
Step 3/9 : RUN [ ! -e /etc/nsswitch.conf ] && echo 'hosts: files dns' > /etc/nsswitch.conf
Step 4/9 : ENV GOLANG_VERSION 1.10.3
Step 5/9 : RUN set -eux;        apk add --no-cache --virtual .build-deps                bash            gcc             musl-dev                openssl           Step 6/9 : ENV GOPATH /go
Step 7/9 : ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH
Step 8/9 : RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"
Step 9/9 : WORKDIR $GOPATH
Successfully built 36b255960f85
Successfully tagged my-golang-app:latest
```

```
tamuragyonoMacBook-puro:go-practice-01 tamuragyo$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
my-golang-app       latest              36b255960f85        2 minutes ago       394MB
alpine              3.8                 11cd0b38bc3c        4 weeks ago         4.41MB
```

```
tamuragyonoMacBook-puro:go-practice-01 tamuragyo$ docker run -it --rm --name my-running-app my-golang-app
/go #
```

```
/go # touch main.go
/go # vi main.go
/go # go run main.go
TEST
```
