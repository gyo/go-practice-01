# Go 練習用

## Docker で開発環境をつくる

ログは [log.md](log.md)

[library/golang - Docker Hub](https://hub.docker.com/r/library/golang/)

Dockerfile を [1.10/alpine3.8/Dockerfile](https://github.com/docker-library/golang/blob/b7a4a48a142ef2ada9c5794ba054fd008656c779/1.10/alpine3.8/Dockerfile) から入手する

```
docker build -t my-golang-app .
```

```
docker run -it --rm --name my-running-app my-golang-app
```

main.go を作成し、`go run main.go` で実行する

main.go

```go
package main

func main() {
  println("TEST")
}
```

```
go run main.go
```

## ホストとコンテナでディレクトリを共有する

```
docker run -it -v /Users/tamuragyo/workspace/go-practice-01/go:/go --rm --name my-running-app my-golang-app
```

## go build について

`go run` で実行できる

```
go run hello.go
```

`go build` すると、バイナリができる

つくったバイナリは実行できる

```
go build hello.go
./hello
```

## Go の勉強

[はじめての Go 言語 - はじめての Go 言語](http://cuto.unirita.co.jp/gostudy/)

### パッケージ

名前空間を分けるための仕組み

次のコードの変数や関数は、全て somepkg 内のメンバ

```go
package somepkg

var SomeVar int
var someVar2 int

func SomeFunc() {
    SomeVar = 10
    someVar2 = 5
}

func someFunc2() {
    SomeFunc()
}
```

他パッケージのメンバにアクセスするには、`import` 文をつかう

GOPATH の src ディレクトリ直下をベースとして、src ディレクトリ内のディレクトリを相対パス指定する。 import 文は指定されたディレクトリ直下のコードのパッケージ名を読み込む

パッケージには別名をつけることもできる

他パッケージから参照が可能なものはパブリックメンバという

名前の頭文字が英字大文字で始まるメンバ

```go
package otherpkg

import some "somepkg"

func OtherFunc() {
    some.SomeFunc()
    some.SomeVar = 5
}
```

#### main パッケージ

main 関数を定義すると、プログラム実行時のエントリポイントになる

`go run`, `go build`, `go install` をするのに必要

#### 循環参照

循環参照はエラーになる

#### ディレクトリへの配置ルール

ひとつのディレクトリには、同じパッケージが宣言されたファイルしか置けない

テストコード（末尾が`_test`で終わるパッケージ）は例外

### GOPATH

直下に、bin, pkg, src ディレクトリを配置する

`go install` は、

> compile and install packages and dependencies

のためのコマンド

`go install ディレクトリパス`

というコマンドで、bin ディレクトリに実行ファイルを作成する

## メモ

```
docker run -it -v /Users/tamuragyo/workspace/go-practice-01/go:/go --rm --name my-running-app my-golang-app
```

[はじめての Go 言語 - はじめての Go 言語](http://cuto.unirita.co.jp/gostudy/)
